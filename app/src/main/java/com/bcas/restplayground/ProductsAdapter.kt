package com.bcas.restplayground

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bcas.restplayground.databinding.ItemProductBinding
import com.bumptech.glide.Glide

class ProductsAdapter : RecyclerView.Adapter<ProductsAdapter.ProductViewHolder>() {

    private var onClick : (ProductResponse) -> Unit={}
    private var listProduct : MutableList<ProductResponse> = mutableListOf()

    inner class ProductViewHolder(
        private val binding : ItemProductBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: ProductResponse, onClick: (ProductResponse) -> Unit) {
            binding.tvProductName.text = item.title
            binding.tvProductPrice.text = "Rp${item.price}"

            Glide.with(binding.root.context)
                .load(item.image)
                .circleCrop()
                .into(binding.imgProduct)

            binding.clProduct.setOnClickListener {
                onClick(item)
            }
            }
        }

    private val products : MutableList<ProductResponse> = mutableListOf()

    fun addNewsProducts(newProducts: List<ProductResponse>) {
        products.addAll(newProducts)
        notifyDataSetChanged()
    }

    fun onClickProduct(clickProduct: (ProductResponse)->Unit) {
        onClick = clickProduct
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ItemProductBinding.inflate(inflater, parent, false)
        val viewHolder = ProductViewHolder(binding)
        return viewHolder
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind( products[position], onClick)
//        holder.bind(product)
    }
}